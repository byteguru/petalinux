#!/bin/bash
source /opt/pkg/petalinux/settings.sh
petalinux-package --boot --force --fsbl images/linux/zynq_fsbl.elf --fpga /home/test/testBoot/design/system_wrapper.bit --u-boot
mkdir /media/test/ZYBO_BOOT
mount /dev/sdb1 /media/test/ZYBO_BOOT

cp images/linux/BOOT.BIN /media/test/ZYBO_BOOT/
cp images/linux/image.ub /media/test/ZYBO_BOOT/

sleep 1
umount /dev/sdb1
rm /media/test/ZYBO_BOOT -R
